# an OpenShift observer

This is a tiny utility to observe changes of OpenShift objects and send them to a Apache Kafka topic.

A Docker container image is available as `docker.io/goern/observe-kafka:v3.6.0-dev`, it includes tools
from OpenShift v3.6 and https://github.com/Shopify/sarama/

Environment variables and volumes
----------------------------------

|    Variable name       |    Description                                |
| :--------------------- | --------------------------------------------- |
|  `KAFKA_PEER`          | kafka broker to be used, default: kafka:9092  |
|  `TOPIC`               | topic to send to, default: eventrouter        |


## Build

To build a container image on your local OpenShift use:

```
oc new-build --binary --strategy=docker --name observe-imagestreams --to=observe-imagestreams
oc start-build observe-imagestreams --from-dir=.
```

`from-dir` is the directory where this README is at.


## Debugging Kafka Topics

/opt/kafka_2.11-0.11.0.0/bin/kafka-console-consumer.sh --bootstrap-server=kafka:9092 --topic openshift-observe-imagestreams --from-beginning

