#!/bin/bash

brokers=${KAFKA_PEER:-kafka:9092}
topic=${TOPIC:-openshift.raw}

echo -n "{\"verb\":\"CHANGED\",\"event\":{\"involvedObject\":{\"kind\":\"ImageStream\",\"namespace\":\"${1}\",\"name\":\"${2}\"}}}" | kafka-console-producer -verbose -brokers ${brokers} -topic ${topic}
