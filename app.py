#!/bin/env python

from __future__ import with_statement, print_function

from kafka import KafkaConsumer, KafkaProducer
from kafka.errors import KafkaError

import requests
import json
import logging
import os


DEBUG_LOG_LEVEL = bool(os.getenv('DEBUG', False))
OPENSHIFT_API_ENDPOINT = 'https://127.0.0.1:8443/oapi/v1/'
BEARER_TOKEN = os.getenv(
    'BEARER_TOKEN', 'xvB24rtp6m0w6g-znfG6tC7PodojqAa1Cx48WSJ3H_g')
RAW_TOPIC_NAME = bool(
    os.getenv('RAW_TOPIC_NAME', 'openshift.raw'))
IMAGESTREAM_TOPIC_NAME = bool(
    os.getenv('IMAGESTREAM_TOPIC_NAME', 'openshift.imagestreams'))


class ImageStreamConsumer:

    def __init__(self, endpoint, token, bootstrap_servers=['localhost:9092']):
        self.endpoint = endpoint
        self.token = token

        self.bootstrap_servers = bootstrap_servers

        self.consumer = KafkaConsumer(RAW_TOPIC_NAME,
                                      value_deserializer=lambda m: json.loads(
                                          m.decode('ascii')),  # TODO check if we really send ascii
                                      bootstrap_servers=self.bootstrap_servers)

        self.producer = KafkaProducer(bootstrap_servers=self.bootstrap_servers,
                                      value_serializer=lambda m: json.dumps(
                                          m).encode('ascii'),  # TODO check if we really send ascii
                                      retries=5)

    def consume(self):
        for message in self.consumer:
            logger.debug("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                                        message.offset, message.key,
                                                        message.value))

            if message.value["event"]["involvedObject"]["kind"] != "ImageStream":
                continue

            changedImageStream = message.value["event"][
                "involvedObject"]  # this is a dict/JSON Object
            logger.debug("Received: %s" % changedImageStream)

            # TODO if we timeout on this request, do not ack the message
            headers = {
                "Authorization": "Bearer %s" % self.token}

            try:
                imageStream = requests.get(self.endpoint + "namespaces/%s/imagestreams/%s" % (changedImageStream["namespace"], changedImageStream['name']),
                                           headers=headers, verify=False)

                if imageStream.status_code == 401:
                    logger.warn("user is not authorized to view namespaces/%s/imagestreams/%s" %
                                (changedImageStream["namespace"], changedImageStream['name']))
                    continue

                imageStreamJSON = imageStream.json()
            # TODO except more specific
            except Exception as e:
                logger.debug(self.endpoint + "namespaces/%s/imagestreams/%s" % (changedImageStream["namespace"], changedImageStream['name']),
                             headers=headers, verify=False)
                logger.error(e)
                continue

            logger.debug("ImageStream: %s - %s" %
                         (changedImageStream, imageStreamJSON))

            self.producer.send(IMAGESTREAM_TOPIC_NAME, imageStreamJSON)
            self.producer.flush()


if __name__ == "__main__":
    if DEBUG_LOG_LEVEL:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info('initializing...')

    # if we can read bearer token from /var/run/secrets/kubernetes.io/serviceaccount/token use it,
    # otherwise use the one from env
    try:
        logger.debug(
            'trying to get bearer token from secrets file within pod...')
        with open('/var/run/secrets/kubernetes.io/serviceaccount/token') as f:
            BEARER_TOKEN = f.read()

        # if we can read bearer token from file, use openshift.svc as anedpoint
        # otherwise use localhost
        OPENSHIFT_API_ENDPOINT = 'https://openshift.default.svc.cluster.local/oapi/v1/'

    except:
        logger.info("not running within an OpenShift cluster...")

    isc = ImageStreamConsumer(OPENSHIFT_API_ENDPOINT, BEARER_TOKEN,
                              os.getenv('KAFKA_PEERS', 'localhost:9092'))
    isc.consume()
